import {createStore, combineReducers} from 'redux'
import settings from './settings/reducer'

const reducer = combineReducers({
  settings
})

const store = createStore(reducer)

export default store