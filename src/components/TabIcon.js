import React from 'react'
import Iicon from 'react-native-vector-icons/Ionicons'
import Ficon from 'react-native-vector-icons/FontAwesome'
import Micon from 'react-native-vector-icons/MaterialIcons'

export default class TabIcon extends React.Component {
  render() {
    const {type, focused, activeTintColor, inactiveTintColor, ...props} = this.props
    const settings = {
      size: 28,
      color: focused ? activeTintColor : inactiveTintColor
    }
    return (
      type === 'i' ? <Iicon {...settings} {...props}/> :
      type === 'm' ? <Micon {...settings} {...props}/> :
      type === 'f' ? <Ficon {...settings} {...props}/> :
        <Iicon name='ios-more'/>
    );
  }
}