import React from 'react'
import Icon from './../TabIcon'

export default class VerticalToggle extends React.Component {
  render() {
    return (
      <View>
        <View>
          <Icon name='ios-calendar-outline'/>
        </View>
        <View>
          <Icon name='ios-apps'/>
        </View>
      </View>
    )
  }
}