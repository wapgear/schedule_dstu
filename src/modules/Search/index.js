import SearchModule from './SearchModule'
import tabs from './_tabs'

export {
  SearchModule,
  tabs
}