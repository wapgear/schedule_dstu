import PersonalModule from './PersonalModule'
import tabs from './_tabs'

export {
  PersonalModule,
  tabs
}