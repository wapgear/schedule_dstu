import React from 'react'
import {View, Text} from 'react-native'
import colors from './../../helpers/colors'

const Item = props => {
  return (
    <View style={styles.item.container}>
      <Text style={styles.item.name}>Item title</Text>
    </View>
  )
}

export default class PersonalMenu extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Group info</Text>
        <Text style={styles.group}>Your group: УНЭ42</Text>
        <View style={styles.button.container}>
          <Text style={styles.button.text}>Change group</Text>
        </View>
      </View>
    )
  }
}

const styles = {
  container: {
    marginTop: 25,
    paddingLeft: 15
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
  },
  group: {
    fontSize: 24
  },
  button: {
    container: {
      alignItems: 'flex-start'
    },
    text: {
      fontSize: 18,
      color: colors.main,
    }
  },
  item: {
    container: {
      borderTopWidth: 1,
      borderTopColor: colors.fifth,
      paddingVertical: 5,
      paddingHorizontal: 10
    },
    name: {
      fontSize: 18
    }
  }
}