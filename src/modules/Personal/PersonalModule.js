import React from 'react'
import {ScrollView, View, Text} from 'react-native'
import VerticalToggle from './VerticalToggle'
import DayModule from './DayModule'
import Elements from './Menu'

export default class PersonalModule extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          {/*<VerticalToggle />*/}
          <DayModule />
          <Elements />
        </View>
      </ScrollView>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 50,
    alignItems: 'stretch'
  },
}