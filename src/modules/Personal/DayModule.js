import React from 'react'
import {View, Text} from 'react-native'
import colors from './../../helpers/colors'
import Icon from './../../components/TabIcon'

const Title = () => {
  return (
    <View style={styles.title.container}>
      <View style={[styles.title.el, styles.title.back]}>
        <Icon name={'ios-arrow-back'} type={'i'}/>
      </View>
      <View style={[styles.title.el, styles.title.nameEl]}>
        <Text style={styles.title.text}>MONDAY</Text>
      </View>
      <View style={[styles.title.el, styles.title.forward]}>
        <Icon name={'ios-arrow-forward'} type={'i'} />
      </View>
    </View>
  )
}

const Item = (props) => {
  return (
    <View style={[styles.item.container, props.active ? styles.item.active : null]}>
      <View style={styles.item.row}>
        <Text style={styles.item.name}>
          Освещение церкви в тех вузе
        </Text>
      </View>
      <View style={styles.item.row}>
        <Text style={styles.item.timeStart}>
          8:30 - 10:05
        </Text>
        <Text style={styles.item.auditory}>
          а8-504
        </Text>
        <Text style={styles.item.teacher}>
          Попович П. П.
        </Text>
      </View>
    </View>
  )
}

export default class DayModule extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Title />
        <Item active/>
        <Item />
        <Item />
        <Item />
        <Item />
      </View>
    )
  }
}

const styles = {
  container: {
  },
  item: {
    container: {
      borderTopWidth: 0,
      borderTopColor: colors.fifth,
      borderBottomWidth: 1,
      borderBottomColor: colors.fifth,
      paddingVertical: 10,
      marginBottom: 5,
      paddingHorizontal: 10
    },
    active: {
      backgroundColor: colors.fifth
    },
    row: {
      flexDirection: 'row'
    },
    timeStart: {
      fontWeight: '500',
      fontSize: 18,
      marginRight: 10
    },
    name: {
      fontSize: 20,
      marginBottom: 5
    },
    auditory: {
      fontWeight: 'bold',
      fontSize: 18,
      marginRight: 5
    },
    teacher: {
      fontSize: 18
    }
  },
  title: {
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderTopWidth: 1,
      borderTopColor: colors.fifth,
      borderBottomWidth: 1,
      borderBottomColor: colors.fifth,
      flexDirection: 'row',
    },
    el: {
      flex: 1,
      justifyContent: 'center'
    },
    back: {
      alignItems: 'flex-start'
    },
    nameEl: {
      alignItems: 'center'
    },
    forward: {
      alignItems: 'flex-end'
    },
    text: {
      fontSize: 20
    }
  }
}