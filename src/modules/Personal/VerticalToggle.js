import React from 'react'
import Icon from './../../components/TabIcon'
import {View} from 'react-native'
import colors from './../../helpers/colors'

export default class VerticalToggle extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.item}>
          <Icon name='ios-calendar-outline' size={36} type='i' color={'#b6b6b6'}/>
        </View>
        <View style={styles.item}>
          <Icon name='ios-apps' size={36} type='i' color={colors.main}/>
        </View>
      </View>
    )
  }
}

const styles = {
  container: {
    height: 75,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  item: {
  }
}