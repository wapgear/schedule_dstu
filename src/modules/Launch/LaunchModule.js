import React from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import colors from './../../helpers/colors'

export default class LaunchModule extends React.Component {
  onPress = () => {
    Actions.personal()
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Launch screen</Text>
        <Text style={styles.welcomeDesc}>Some text put here</Text>
        <TouchableOpacity
          onPress={this.onPress}
        >
          <Text style={styles.button}>
            Start using dat shit
          </Text>
        </TouchableOpacity>

      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  welcome: {
    color: colors.main,
    fontSize: 32
  },
  welcomeDesc: {
    color: colors.third,
    fontSize: 20
  },
  button: {
    marginTop: 100,
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 28,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    color: colors.main
  }
};
