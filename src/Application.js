import React from 'react';
import {Provider} from 'react-redux'
import {Router, Scene, Stack, Tabs} from 'react-native-router-flux'
import {View, Text} from 'react-native'
import store from './store'
import Launch from './modules/Launch'
import {PersonalModule, tabs as PersonalTabs} from './modules/Personal'
import {SearchModule, tabs as SearchTabs} from './modules/Search'
import Icon from 'react-native-vector-icons/Ionicons';
import Micon from 'react-native-vector-icons/MaterialIcons';
import colors from './helpers/colors'
import TabIcon from './components/TabIcon'

const tabStyle = {
  fontSize: 14,
}


export default class Application extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Stack key="root">
            <Scene
              key="launch"
              component={Launch}
              title="Launch Window"
              hideNavBar
            />
            <Scene
              tabs
              key="main"
              hideNavBar
              labelStyle={tabStyle}
              activeTintColor={colors.main}
              inactiveTintColor={colors.third}
            >
              <Scene
                key="search"
                component={SearchModule}
                title="Search"
                icon={props =>
                  <TabIcon
                    name='ios-search'
                    type='i'
                    {...props}
                  />
                }
              />
              <Scene
                key="personal"
                component={PersonalModule}
                title="My schedule"
                icon={props =>
                  <TabIcon
                    name='favorite-border'
                    type='m'
                    {...props}
                  />
                }
              />
            </Scene>
          </Stack>
        </Router>
      </Provider>
    );
  }
}